"""
Script per il conteggio, poligono per poligono, dei punti per poligono
Data creazione: 27/12/2015
Data ultima modifica: 07/05/2017
Autore: Alessandro Lucchet
"""
import tempfile
import sys
import os

# Costanti
SCRIPT_PATH=os.path.dirname(os.path.realpath(__file__))+'\\'
POLYGON_TAG = "Polygon"
POINT_TAG = "Point"
COORDS_TAG = "coordinates"
NAME_TAG = "name"
VISIBILITY_TAG = "visibility"
PLACEMARK_TAG = "Placemark"
COLOR_PARAM = '\033[92m'
COLOR_END = '\033[0m'

# include
from xml.dom.minidom import parse,Document
from subprocess import call

# funzione per controllare se un punto si trova dentro un poligono
def point_in_poly(x,y,poly):

	# utilizza i max e min per capire se si trova fuori o dentro
    if x>max_lng or x<min_lng or y>max_lat or y<min_lat:
	    return False

    n = len(poly)
    inside = False

    p1x,p1y = poly[0]
    for i in range(n+1):
        p2x,p2y = poly[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xints:
                        inside = not inside
        p1x,p1y = p2x,p2y

    return inside

# inizio programma
os.system("cls")
print("Questo script:\n1. Apre un file KML\n2. Rileva tutti i segnaposto inseriti nel KML\n\tNota 1: i segnaposto devono contenere (nel nome) il numero delle famiglie residenti in quel punto\n\tNota 2: per velocizzare il passaggio successivo, la rilevazione dei segnaposto viene fatta solo alla prima esecuzione di questo script. Se si modificano i segnaposto, bisogna riavviare lo script\n3. Per ogni poligono (area) visibile all'interno del KML, conteggia le famiglie e mostra i risultati\n4. Ripete il punto 3 all'infinito\n")
if len(sys.argv)>1:
	filekml=sys.argv[1]
	print("Utilizzerò il seguente file KML: "+COLOR_PARAM+filekml+COLOR_END+"\n")
else:
	# print("Inserisci il nome del file da aprire: ")
	# filekml = input()
	filekml = os.getenv('LOCALAPPDATA')+"Low\\Google\\GoogleEarth\\myplaces.kml"
	print("Utilizzerò il file KML di Google Earth \"I miei luogi\".\nQuesto file si trova in: "+COLOR_PARAM+filekml+COLOR_END+"\nIn alternativa puoi trascinare sopra questo script il file KML da conteggiare\n")
os.system('pause')
xmldoc = parse(filekml)
list = xmldoc.getElementsByTagName(PLACEMARK_TAG)

# estraggo una lista dei punti
points = []  #dichiaro la lista
for placemark in list:
	if len(placemark.getElementsByTagName(POINT_TAG))>0:
		
		# estraggo il punto
		vertici=placemark.getElementsByTagName(COORDS_TAG)[0].firstChild.nodeValue.split()
		
		# estraggo quanto vale
		if len(placemark.getElementsByTagName(NAME_TAG))>0:
			name=placemark.getElementsByTagName(NAME_TAG)[0].firstChild.nodeValue
		else:
			name="0"
			
		# aggiungo il punto
		coordinata=vertici[0].split(',')
		try:
			quantita=int(name)
		except ValueError:
			quantita=0
		points.append([float(coordinata[0]),float(coordinata[1]),quantita])
			
# estraggo i poligoni, e per ogni poligono conto
while True:
	os.system("cls")
	count = 0
	for placemark in list:
		if len(placemark.getElementsByTagName(POLYGON_TAG))>0 and (len(placemark.getElementsByTagName(VISIBILITY_TAG))==0 or placemark.getElementsByTagName(VISIBILITY_TAG)[0].firstChild.nodeValue==1):
			count += 1
			
			# estraggo il nome (se c'e')
			if len(placemark.getElementsByTagName(NAME_TAG))>0:
				name=placemark.getElementsByTagName(NAME_TAG)[0].firstChild.nodeValue
			else:
				if len(placemark.parentNode.getElementsByTagName(NAME_TAG))>0:
					name=placemark.parentNode.getElementsByTagName(NAME_TAG)[0].firstChild.nodeValue
				else:
					name='poligono_'+str(count)
					
			# scorro i vertici (e nel frattempo calcolo max_lat,min_lat,max_lng,min_lng)
			polygon=[]
			max_lat = -999
			max_lng = -999
			min_lat = 999
			min_lng = 999		
			vertici=placemark.getElementsByTagName(COORDS_TAG)[0].firstChild.nodeValue.split()
			for vertice in vertici:
				# aggiungo l'elemento che contiene le coordinate
				coordinata=vertice.split(',')
				polygon.append([float(coordinata[0]),float(coordinata[1])])
							
				if min_lat > float(coordinata[1]):
					min_lat = float(coordinata[1])
				if max_lat < float(coordinata[1]):
					max_lat = float(coordinata[1])
				if min_lng > float(coordinata[0]):
					min_lng = float(coordinata[0])
				if max_lng < float(coordinata[0]):
					max_lng = float(coordinata[0])
				
				
			# calcolo quanti punti sono all'interno del poligono
			conteggio=0
			for punto in points:
				if point_in_poly(punto[0],punto[1],polygon):
					conteggio += punto[2]
			
			print(name+": "+str(conteggio)+" famiglie")


	os.system('pause')
	xmldoc = parse(filekml)
	list = xmldoc.getElementsByTagName(PLACEMARK_TAG)